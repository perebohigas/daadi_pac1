//
//  CItemProvider.swift
//  PR1S
//
//  Created by Javier Salvador Calvo on 24/9/16.
//  Copyright © 2016 UOC. All rights reserved.
//

import Foundation
import UIKit

class CItemProvider : NSObject, XMLParserDelegate
{

    var m_delegate:ViewController? = nil;
    
    var m_parser:XMLParser? = nil;
 
    // Extra properties for loading and parsing the items list
    let webURLstring = "http://einfmlinux1.uoc.edu/devios/data.xml"
    var foundCharacters = ""
    var currentItem: CItemData? = nil
    var tagShouldHaveContent = false
    //
    
    var m_items:NSMutableArray?=nil;
    var m_loading:Bool = false
    
    
    
    // ---------------------------------
 
    func GetItemAt(position:CLong) -> CItemData
    {
        return self.m_items?[position] as! CItemData
    }
    
    
    func GetCount() -> CLong
    {
        return self.m_items!.count;
    }

     // ---------------------------------
    
    
    
    func LoadAndParse()
    {
        self.m_loading = false;
        
        self.performSelector(inBackground: #selector(LoadAndParseInternal), with:nil)
    
    }
    
    
    @objc func LoadAndParseInternal()
    {
    
        // Exercise 4.1
        //self.LoadAndParseHardcoded()
        
        self.LoadAndParseFromWeb();

        
        self.m_loading = true;
        
        
    
        
        self.m_delegate?.performSelector(onMainThread: #selector(ViewController.LoadingEnd), with: nil, waitUntilDone: false)
        
        
     
    }
    
    
    func LoadAndParseHardcoded()
    {
    
    
    self.m_items = NSMutableArray(capacity: 6)
    
    var item:CItemData;
    
        
    item = CItemData(id: "00000001",title: "Item 1",type: 1)
    self.m_items?.add(item)
        
    item = CItemData(id: "00000002",title: "Item 2",type: 2)
    self.m_items?.add(item)
    
    item = CItemData(id:"00000003",title: "Item 3",type: 3)
    self.m_items?.add(item)
    
    item = CItemData(id: "00000004",title: "Item 4",type: 4)
    self.m_items?.add(item)

        
    item = CItemData(id: "00000005",title: "Item 5",type: 5)
    self.m_items?.add(item)

    item = CItemData(id: "00000006",title: "Item 6",type: 6)
    self.m_items?.add(item)

        
    }
    
    
    func LoadAndParseFromWeb()
    {
    // BEGIN-CODE-UOC-2
    
        // Exercise 4.2.1
        // Download the xml file synchronously from web
        var xmlData:Data?
        if let url = URL(string: webURLstring) {
            do {
                xmlData = try Data(contentsOf: url)
            } catch {
                print("Error: Data file could not be download from web")
            }
        } else {
            print("Error: Wrong URL")
        }
        
        // Exercise 4.2.2
        // Parse xml file with items list
        if let itemsListData = xmlData {
            m_parser = XMLParser(data: itemsListData)
            m_parser?.delegate = self
            m_parser?.parse()
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        switch elementName {
        case "items":
            m_items = NSMutableArray.init()
            print("Start parsing the items list from \(webURLstring)")
        case "item":
            // Start parsing an item
            currentItem = CItemData.init()
            if (!attributeDict.isEmpty){
                currentItem?.m_id = attributeDict["id"] ?? "no id"
                currentItem?.m_type = CLong(attributeDict["type"]!) ?? 0
            }
        case "title", "description", "data":
            // Start parsing a tag
            tagShouldHaveContent = true
            foundCharacters = ""
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if (tagShouldHaveContent) {
            foundCharacters += string.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        tagShouldHaveContent = false
        switch elementName {
        case "title":
            currentItem?.m_title = foundCharacters
            // Finish parsing a "title" tag
        case "description":
            currentItem?.m_description = foundCharacters
            // Finish parsing a "description" tag
        case "data":
            currentItem?.m_data = foundCharacters
            // Finish parsing a "data" tag
        case "item":
            print("Item parsed [id: \(currentItem!.m_id), type: \(currentItem!.m_type), title: \(currentItem!.m_title), description: \(currentItem!.m_description), data: \(currentItem!.m_data)]")
            if let item = currentItem {
                m_items?.add(item)
            }
            // Finish parsing an item
        case "items":
            print("Finish parsing the items list from \(webURLstring)")
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("Error: An error occurred during parsing")
    
    // END-CODE-UOC-2
    
    }

    
    // *****************************************
    

}



